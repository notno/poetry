FROM python:slim
RUN pip install poetry
RUN poetry config virtualenvs.create false
