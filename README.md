# poetry

minimal python:slim + poetry docker image

## ..but why?

* poetry install takes time, we can shave it off by reusing an image
* to be used to convert pyproject.toml dependencies into requirements.txt within Dockerfiles


## usage
```
FROM registry.gitlab.com/notno/poetry as poetry
COPY pyproject.toml /pyproject.toml
COPY poetry.lock /poetry.lock
RUN poetry config virtualenvs.create false
RUN poetry export --without-hashes --output /requirements.txt
RUN poetry export --dev --without-hashes --output /requirements-dev.txt

FROM whatever
COPY --from=poetry /requirements.txt
RUN pip install -r requirements.txt
```
